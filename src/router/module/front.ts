import type { RouteRecordRaw } from 'vue-router';

const routes: RouteRecordRaw[] = [
    {
        path: '/front',
        component: () => import('../../pages/Front.vue'),
    },
];

export default routes;